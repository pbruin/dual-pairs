Elements of finite flat algebras
================================

.. automodule:: dual_pairs.finite_flat_algebra_element
   :members:
