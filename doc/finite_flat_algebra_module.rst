Modules over finite flat algebras
=================================

.. automodule:: dual_pairs.finite_flat_algebra_module
   :members:
