Dual pairs of algebras over the rationals
=========================================

.. automodule:: dual_pairs.dual_pair_rational
   :members:
