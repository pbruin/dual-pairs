Selmer groups of finite flat algebras
=====================================

.. automodule:: dual_pairs.selmer_group
   :members:
