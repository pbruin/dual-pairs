Utility functions for finite Abelian groups
===========================================

.. automodule:: dual_pairs.group_structure
   :members:
