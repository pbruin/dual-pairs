Unit groups of finite flat algebras
===================================

.. automodule:: dual_pairs.unit_group
   :members:
