Finite flat algebras
====================

.. automodule:: dual_pairs.finite_flat_algebra
   :members:
